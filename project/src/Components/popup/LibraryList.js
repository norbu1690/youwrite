import { StyleSheet, Text, View, TouchableOpacity, Modal, Button, Alert, FlatList} from 'react-native'
import React, { useState, useEffect } from 'react'
import firebase from '../../Firebase/config';

const LibraryList = ({navigation}) => {
    const [modalVisible, setModalVisible] = useState(false);
    const showModal1=()=>{
        setModalVisible(true)
    }

    const [name, setName] = useState(''); 
    const [readingList, setReadingList] = useState([]); 
    const listRef = firebase.firestore().collection('readingList');

    useEffect(() => {

        listRef
            .orderBy('createdAt', 'desc')
            // fetch articles in realtime
            .onSnapshot(
                querySnapshot => {
                    const newList = []
                    // loop through the saved articles
                    querySnapshot.forEach(doc => {
                        const name = doc.data()
                        name.id = doc.id
                        newList.push(name)
                    });
                    // set an article to the state
                    setReadingList(newList)
                },
                error => {
                    console.error(error);
                }
            );
    }, []);

    const check = () => {
        Alert.alert(
            "Adding to a reading list!",
            'Are you sure?',
            [
              {text: 'Yes', onPress: () =>  Alert.alert("Success message!","Successfully added")},
              {text: 'No', onPress: () => console.log('Not added'), style: 'cancel'},
            ],
            { 
              cancelable: true 
            }
          );
    }

    const renderReadingList = ({ item }) => {
        return (
            <TouchableOpacity onPress={check}>
                <View style={styles.articleContainer} >
                        <Text style={styles.articleText}>
                            {item.title[0].toUpperCase() + item.title.slice(1)}
                        </Text>
                </View>
                <View>
                </View>
            </TouchableOpacity>
        )
    }

  return (
    <View style={{ justifyContent: 'center', alignItems: 'center'}}>
        <Modal
          animationType="fade"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            setModalVisible(!modalVisible);
          }}
        >
            <View style={styles.modalView}>
                <View style={styles.container}>
                    <Text style={{fontWeight: 'bold'}}>
                        Select the reading List
                    </Text>
                </View>
                <View style={ styles.container}>
                {readingList.length > 0 && (
                    <View>
                        <FlatList
                            data={readingList}
                            renderItem={renderReadingList}
                            keyExtractor={(name) => name.id}
                            removeClippedSubviews={true}
                        />
                    </View>
                )}
                </View>
                <Button
                    color='#2F3D4F'
                    title='Cancel'
                    style={[styles.button, styles.buttonClose]}
                    onPress={() => setModalVisible(!modalVisible)}
                />
            </View>  
        </Modal>
    <View>
    </View>
    <View style={{width: 90}}>
        <View style={styles.page}>
            <TouchableOpacity onPress={showModal1}>
                <Text style={styles.next}>ADD</Text>
            </TouchableOpacity>
        </View>
    </View>   
    </View>
  )
}
export default LibraryList;

const styles = StyleSheet.create({
    page:{
        backgroundColor:'#F26349',
        height:43,
        borderRadius: 5,
        width:"100%",
        shadowColor:'red',
        shadowRadius:10,
        shadowColor: 'black',
        shadowOffset: {width: 8, height: 9},
        shadowRadius: 6,
        shadowOpacity: 0.9,
        padding: 5,
        elevation: 2,
        paddingTop: 10
    },
    container:{
        flexDirection:'row',
        width:'100%',
        justifyContent:'space-around',
        marginBottom:10,
    },

    modalText: {
        fontSize:20,
        fontWeight:'bold',
        marginBottom: 15,
        textAlign: "center"
    },
    modalView: {
        width:300,
        backgroundColor: "#F26349",
        borderRadius: 20,
        padding:20,
        marginTop:200,
        justifyContent: 'center',
        alignSelf: 'center',
        shadowColor: "#696665",
        shadowOffset: {
            width: 0,
            height: 2
        },
        alignItems:'center',
        borderWidth: 2
    },
    next:{
        textAlign: 'center',
        fontSize:16,
        color: 'white',
        fontWeight: 'bold',
        
    },
    node:{
        color:'black'
    }
  })