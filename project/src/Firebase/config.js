import firebase from 'firebase/compat';
import 'firebase/compat/storage';
import firestore from 'firebase/compat/firestore';
import 'firebase/auth'

const firebaseConfig = {
  apiKey: "AIzaSyA-UrCi-_W6nHAjp2iNtWhDh8N3VgP6SZc",
  authDomain: "youwrite-9af11.firebaseapp.com",
  databaseURL: "https://youwrite-9af11-default-rtdb.firebaseio.com",
  projectId: "youwrite-9af11",
  storageBucket: "youwrite-9af11.appspot.com",
  messagingSenderId: "957205744270",
  appId: "1:957205744270:web:0b13cf23cbd822291497df",
  measurementId: "G-S6NC9WG9VN"
};

firebase.initializeApp(firebaseConfig);
firebase.firestore();
export const auth = firebase.auth();

const storage = firebase.storage();

export {storage, firebase as default}
