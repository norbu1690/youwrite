import React, { useState, useEffect } from 'react';
import { SafeAreaView, StyleSheet, View, Text, TouchableOpacity, FlatList} from 'react-native';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/FontAwesome';
import firebase from '../../Firebase/config';
import { FontAwesome } from "@expo/vector-icons";

export default function Library({navigation})  {
    const [name, setName] = useState(''); 
    const [readingList, setReadingList] = useState([]); 
    const listRef = firebase.firestore().collection('readingList');

    useEffect(() => {

        listRef
            .orderBy('createdAt', 'desc')
            // fetch articles in realtime
            .onSnapshot(
                querySnapshot => {
                    const newList = []
                    // loop through the saved articles
                    querySnapshot.forEach(doc => {
                        const name = doc.data()
                        name.id = doc.id
                        newList.push(name)
                    });
                    // set an article to the state
                    setReadingList(newList)
                },
                error => {
                    // log any error
                    console.error(error);
                }
            );
    }, []);

    const renderReadingList = ({ item }) => {
        return (
            <TouchableOpacity>
                <View style={styles.articleContainer} >
                    {/* <ImageBackground source={{ uri: url }} style={styles.images}> */}
                        <Text style={styles.articleText}>
                            {item.title[0].toUpperCase() + item.title.slice(1)}
                        </Text>
                        <FontAwesome name="trash-o" color="red" onPress={() => deleteReadingList(item)} style={styles.deleteIcon} />
                    {/* </ImageBackground> */}
                </View>
                <View>
                </View>
            </TouchableOpacity>
        )
    }
    
    const deleteReadingList = (name) => {
        listRef
            .doc(name.id)
            .delete()
            .then(() => {
                alert("Deleted successfully");
            })
            .catch(error => {
                alert(error);
            })
    }
  return (
    <SafeAreaView style={styles.container}>
        <View style={styles.libraryView}>
        {readingList.length > 0 && (
                    <View style={styles.listContainer}>
                        <FlatList
                            data={readingList}
                            renderItem={renderReadingList}
                            keyExtractor={(name) => name.id}
                            removeClippedSubviews={true}
                        />
                    </View>
                )}
        </View>  
        <View style={styles.container}>
            <ActionButton buttonColor="#F26349">
            <ActionButton.Item
                buttonColor="#9b59b6"
                title="Create a new reading list"
                onPress={()=>navigation.replace("CreateNewLibrary")}
                >
                <Icon name="book" style={styles.actionButtonIcon} />
            </ActionButton.Item>
            </ActionButton>
        </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        // padding: 10,
        width: '100%'
    },
    actionButtonIcon: {
        fontSize: 20,
        height: 22,
        color: 'white',
    },
    libraryView: {
        maxWidth: '100%',
        maxHeight: '80%',
        height: '80%',
        marginTop: 2,
    },
    articleContainer: {
        flex:1,
        flexDirection:'column',
        height: 150,
        width: '90%',
        margin: '3%',
        backgroundColor: '#d2d4d6',
        borderRadius: 10,
        shadowColor:"#000",
        shadowOffset: { width:0, height:2 }, 
        shadowOpacity:0.25, 
        shadowRadius:3.84, 
        elevation:10,
    },
    articleText: {
        fontSize: 16,
        color: 'black',
        textAlign: 'center',
    },
    deleteIcon: {
        fontSize: 24,
        alignSelf: 'flex-end',
        marginTop:'30%',
        marginRight:5
    }
});