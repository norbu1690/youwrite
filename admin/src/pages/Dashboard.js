import React, {useState, useEffect} from 'react'
import fireDb from '../firebase';
import { Link } from 'react-router-dom';
import './Dashboard.css'
import { toast } from 'react-toastify';


const Dashboard = () => {
  const [data, setData] = useState({});

  useEffect (()=> {
    fireDb.child("users").on("value", (snapshot) => {
      if (snapshot.val() !== null){
        setData({...snapshot.val() });
      }
      else {
        setData({});
      }
    });

    return () => {
      setData({});
    }; 
  },[]);

  const onDelete = (id) => {
      if (window.confirm("Are you sure you want to delete this user ?")) {
          fireDb.child(`users/${id}`).remove((err) => {
            if(err) {
              toast.error(err)
            }
            else {
              toast.success("User deleted successfully.")
            }
          })
      }
  }

  return (
   <div style={{marginTop: "80px",}}
   className="container" >
     <table className='styled-table'>
          <thead>
            <tr>
              <th style={{textAlign: "center"}}>Sl.No.</th>
              <th style={{textAlign: "center"}}>Username</th>
              <th style={{textAlign: "center"}}>Email</th>
              <th style={{textAlign: "center"}}>Users Action</th>
              <Link to= "/view">
                          <button className='btn btn-view'>View</button>
                          </Link>

              
            </tr>
          </thead>
          <tbody>
              {Object.keys(data).map((id, index) => {
                return (
                  <tr key={id}>
                      <th scope='row'>{index + 1}</th>
                      <td>{data[id].username}</td>
                      <td>{data[id].email}</td>
                      

                      <td>
                         
                          <button className='btn btn-delete' onClick={()=> onDelete(id)}>Delete</button>
                         
                      </td>
                  </tr>
                );
              })}
          </tbody>   
     </table>
   </div>
  )
}

export default Dashboard
