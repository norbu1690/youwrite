

import React, { useState, useEffect } from "react";
import Container from "react-bootstrap/Container";

import './View.css'
import './Dashboard.css'

import { Link } from "react-router-dom";
import { db } from "../firebase";

import {
  collection,
  deleteDoc,
  doc,
  onSnapshot,
  query,
  where,
} from "firebase/firestore";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
function View() {
  const [users, setUsers] = useState([{ name: "Loading...." }]);
  const usersCollectionRef = collection(db, "Article");
  // const q = query(usersCollectionRef, where());
  const deleteUser = async (id) => {
    const userDoc = doc(db, "Article", id);
    await deleteDoc(userDoc);
  };
  useEffect(() => {
    const unsub = onSnapshot(usersCollectionRef, (snapshot) =>
      setUsers(snapshot.docs.map((doc) => ({ ...doc.data(), id: doc.id })))
    );
    return unsub;
  }, []);
  return (
    <div>
    <Link to="/">
    <button className='btn btn-edit'>Go Back</button>
  </Link>
      <Container className="styled-table">
        <Table hover>
          <thead>
            <tr className='styled-table'>
            <th className="pding">Sl.NO</th>

              <th className="pding">Title</th>
              <th className="pding">Content</th>
              <th className="pding-l"> Action</th>
              <br></br>
            </tr>
          </thead>
          <tbody>
            {users.map((user,index) => (
              
              <tr key={user.id}>
                  <th scope='row' style={{textAlign: "center"}}>{index + 1}</th>
                <td className="pding" style={{textAlign: "center"}}>{user.title}</td>
                <td className="pding"style={{textAlign: "center"}}>{user.content}</td>
               
                <td className="pding-l">
                  <Button className="btn btn-danger cstm"
                    onClick={() => {
                      deleteUser(user.id);
                    }}
                  >
                    Delete
                  </Button>
                  
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
      
    </div>
  );
}
export default View;