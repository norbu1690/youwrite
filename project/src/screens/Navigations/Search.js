import React,{useState,useEffect} from "react"
import {Text,View,StyleSheet, Image, Dimensions, TouchableOpacity, FlatList,} from 'react-native'
import { theme } from "../../core/theme"
import firebase from '../../Firebase/config';
import { ActivityIndicator, Button } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons'
const width = Dimensions.get("screen").width/2-30

export default function Search({navigation, route}){
    const s = route.params
    const [loading, setLoading] = useState(true)
    const [users, setUsers] = useState([])
    const todoRefB = firebase.firestore().collection('Article')

    useEffect(()=>{
        todoRefB
        .onSnapshot(
          query  => {
            const users = []
            query.forEach((doc)=> {
                const {title, content} = doc.data()
                const c = title.charAt(0)
                if(c==s){
                    users.push({
                    id: doc.id,
                    title,
                    content,
                    })
                }
              })
              setUsers(users)
              setLoading(false)
            }
        )
    }, [])
    
    const Refresh=()=>{
        const clonedArr = [...users];
        setUsers(clonedArr)
    }
    return (
        <View style={style.container}>
            <View style={{width: '100%',paddingHorizontal:10,flexDirection:'row',backgroundColor:'#edafa4', height:'15%',paddingTop: 20, justifyContent:'space-between', borderBottomEndRadius: 30, borderBottomStartRadius: 30}}>
                <TouchableOpacity style={{marginTop: 40, flexDirection: 'row'}} onPress={()=>navigation.replace('HomePage')}>
                    <Icon name='keyboard-arrow-left' size={20} />
                    <Text style={{fontWeight:'bold'}}>Go Back</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{marginTop: 40, flexDirection: 'row' }} onPress={Refresh}>
                    <Icon name='refresh' size={20} />
                    <Text style={{fontWeight:'bold'}}>Refresh</Text>
                </TouchableOpacity>
            </View>
            {loading?
            <ActivityIndicator size='large' color='#000ff'/>
                :
            <FlatList
                style={{height:'100%', marginTop: 20, width: '90%'}}
                data={users}
                numColumns={1}
                renderItem={({item})=>(
                    <TouchableOpacity onPress={()=>navigation.navigate("UserDetailScreen", item)}>
                        <View style={style.Cart}>
                            <View style={{height: 125, alignItems: 'center'}}>
                                <Image source={require('../../../assets/nature.jpeg')} style={style.image} />    
                            </View>
                            <Text style={{fontWeight: 'bold', fontSize: 15, marginTop: 10}}>
                               Title: {item.title}
                            </Text>
                        </View>
                    </TouchableOpacity>
                )}
            />
            }
        </View>
    )
}
const style = StyleSheet.create({
    container: {
        flex: 1,
        alignItems:'center',
    },
      Cart: {
          height: 200,
          backgroundColor: '#F26349',
          width: '100%',
          marginHorizontal: 2,
          borderRadius: 10,
          marginBottom: 10,
          padding: 15,
      },
      image: {
        width: '100%',
        height: '100%',
        opacity: 1
    },
})