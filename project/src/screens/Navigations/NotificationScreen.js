import React, { Component } from 'react';
import { StyleSheet, ScrollView, ActivityIndicator, View, Text } from 'react-native';
import { ListItem } from 'react-native-elements'
import firebase from '../../Firebase/config';
import { Ionicons } from '@expo/vector-icons';

class Notification extends Component {
  constructor() {
    super();
    this.firestoreRef = firebase.firestore().collection('Article');
    this.state = {
      isLoading: true,
      userArr: []
    };
  }
  componentDidMount() {
    this.unsubscribe = this.firestoreRef.onSnapshot(this.getCollection);
  }
  componentWillUnmount(){
    this.unsubscribe();
  }
  getCollection = (querySnapshot) => {
    const userArr = [];
    querySnapshot.forEach((res) => {
      const { title, content } = res.data();
      userArr.push({
        key: res.id,
        res,
        title,
        content,
      });
    });
    this.setState({
      userArr,
      isLoading: false,
   });
  }
  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }    
    return (
        <ScrollView style={styles.container}>
          {/* <ExpoNotification /> */}
            {
              this.state.userArr.map((item, i) => {
                return (
                  <ListItem
                    key={i}
                    bottomDivider
                    title={
                      <View style={{flexDirection: 'row'}}><Text style={{width: '90%'}}>You have a new post.</Text>
                        <View style={{}}>
                        <Ionicons name="ios-notifications-sharp" size={24} color="black" />
                        </View>
                      </View>
                    }
                    subtitle={
                      <View>
                        <Text style={{fontWeight: 'bold'}}>
                          {item.title}
                        </Text>
                      </View>
                    }
                    titleStyle={{ color: 'white', fontWeight: 'bold' }}
                
                    leftAvatar={{
                      source: item.avatar_url && { uri: item.avatar_url },
                      title: item.title[0]
                    }}
                 
                    onPress={() => {
                      this.props.navigation.navigate('UserDetailScreen', {
                        userkey: item.key
                      });
                    }}
                    />
                );
              })
            }
        </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingBottom: 22,
        width: '100%',
        alignSelf: 'center',
        marginTop: 10,
    },
    preloader: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center'
    },
})
export default Notification;