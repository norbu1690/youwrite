import React, { Component } from 'react';
import { StyleSheet, ScrollView, ActivityIndicator, View, Text, SafeAreaView } from 'react-native';
import firebase from '../Firebase/config';
import LibraryList from './popup/LibraryList';
import Button from './InnerButton'

class UserDetailScreen extends Component {
  constructor() {
    super();
    this.state = {
      title: '',
      content: '',
      isLoading: true
    };
  }
 
  componentDidMount() {
    const dbRef = firebase.firestore().collection('Article').doc(this.props.route.params.userkey)
    dbRef.get().then((res) => {
      if (res.exists) {
        const article = res.data();
        this.setState({
          key: res.id,
          title: article.title,
          content: article.content,
          isLoading: false
        });
      } else {
        console.log("Document does not exist!");
      }
    });
  }
  
  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }
    return (
      <SafeAreaView style={{ height: '100%'}}>
        <View style={styles.container}>
          <View style={styles.inputGroup}> 
              <Text style={{fontWeight: 'bold', fontSize: 18}}>Title: {this.state.title}</Text>
          </View>
          <ScrollView style={styles.contentView}>
            <Text>{this.state.content}</Text>
          </ScrollView>
          <View style={styles.button}>
            <Button onPress={()=> this.props.navigation.navigate("HomePage")}>Back</Button>
            <LibraryList />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    marginTop: 30,
    height: '100%'
  },
  inputGroup: {
    padding:10,
    marginBottom: 15,
    borderWidth: 1,
    borderColor: '#cccccc',
    borderRadius: 10,
    height: '7%',
    flexDirection: 'row',
  },
  contentView: {
    padding:5,
    marginBottom: 15,
    borderWidth: 1,
    borderColor: '#cccccc',
    borderRadius: 10,
    height: '85%',
    
    
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    marginBottom: 7, 
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    height: '8.75%',
  },
  imageBack: {
    width: 25,
    height: 25,
    marginRight: 10
},
})
export default UserDetailScreen;