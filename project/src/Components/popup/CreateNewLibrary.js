import React, { useState } from 'react' 
import { StyleSheet, Text, View, SafeAreaView,TextInput, Keyboard, Alert } from 'react-native'
import Header from './Header'
import Button from './Button'
import firebase from '../Firebase/config';

const CreateNewLibrary = ({navigation}) => {
    const [name, setName] = useState(''); 
    const [readingList, setReadingList] = useState([]); 
    const listRef = firebase.firestore().collection('readingList');

    const addReadingList = () => {
        if (name && name.length > 0) {
            const timestamp = firebase.firestore.FieldValue.serverTimestamp();
            const data = {
                title: name,
                createdAt: timestamp
            };
            listRef
                .add(data)
                .then(() => {
                    setName('');
                    Keyboard.dismiss(true);
                })
                .catch((error) => {
                    alert(error);
                })
                alert('Successfully done')
                navigation.navigate('Library')
        }
    }
    const check = () => {
        Alert.alert(
            "Leaving the page!",
            "Are you sure you don't want to create a library?",
            [
              {text: 'Yes',onPress: () => navigation.replace("Library")},
              {text: 'No', onPress: () => console.log('Not added'), style: 'cancel'},
            ],
            { 
              cancelable: true 
            }
          );
    }
  return (
   <SafeAreaView style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
       <View style={styles.mainView}>
            <Header>Create a reading list</Header>
                <Text style={{ marginBottom: 5}}>Give your reading list a name</Text>
                <TextInput
                    style={styles.inputText}
                    underlineColor="#F26349"
                    label='name'
                    onChangeText={(title) => setName(title)}
                    value={name}
                    placeholder="eg. My reading collection"
                />
                <View style={{ flexDirection: 'column', maxwidth: '10%'}}>
                    <Button onPress={addReadingList}>Create</Button>
                    <Button onPress={check}>Cancel</Button>
                </View> 
       </View>
   </SafeAreaView>
  )
}

export default CreateNewLibrary

const styles = StyleSheet.create({
    mainView: {
        borderWidth: 1,
        borderRadius: 15,
        height: '50%',
        padding: 5,
        paddingHorizontal: 35,
        maxWidth: '75%',
        position: 'relative'
    },
    inputText: {
        position:'relative',
        borderWidth: 1,
        borderRadius: 10,
        height: 50
    }
})