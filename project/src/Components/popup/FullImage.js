import { StyleSheet, Text, View, TouchableOpacity, Modal, Button, Alert, FlatList, Image} from 'react-native'
import React, { useState, useEffect } from 'react'
import firebase from '../../Firebase/config';
import { getStorage, ref, getDownloadURL } from 'firebase/storage'

const FullImage = ({navigation}) => {
    const [url, setUrl] = useState();

    useEffect(() => {
        const func = async () => {
          const storage = getStorage();
          const reference = ref(storage, 'https://firebasestorage.googleapis.com/v0/b/youwrite-9af11.appspot.com/o/post%2F%24%7Bfirebase.auth().currentUser.uid%7D%2F%24%7BMath.random().toString(36)%7D?alt=media&token=8ae8c95e-e0ca-4dd6-82d1-a7a0be43e04f');
          await getDownloadURL(reference).then((x) => {
            setUrl(x);
          })
        }
    
        if (url == undefined) {func()};
      }, []);

  return (
    <View style={styles.container}>    
       <Image
            style={styles.profilePic}
            source={{ uri: url }}
        />
    </View>
  )
}
export default FullImage;

const styles = StyleSheet.create({
    profilePic: {
        width: '100%',
        height: '50%',
        marginVertical: 50

    },
    container: {
        flex: 1,
        height: '100%',
        width: '100%',
        padding: '1%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'black'
    }
  })