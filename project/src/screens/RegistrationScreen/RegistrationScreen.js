import React, {useState}from 'react';
import {View, StyleSheet,Text, Alert,ScrollView, TouchableOpacity} from 'react-native';
import Header from '../../Components/Header';
import Textinput from '../../Components/Textinput'
import Button from '../../Components/Button';
import { theme } from '../../core/theme';
import { Formik } from 'formik';
import * as yup from 'yup';
import 'react-native-gesture-handler';
import Firebase from '../../Firebase/config';
import Checkingbox  from '../../Components/CheckBox';

const reviewSchema = yup.object({
    username:
     yup
     .string()
     .required('Username is Required')
     .min(4),
    email:
    yup
    .string()
    .email('Please Enter Valid Email')
    .required('Email is Required'),
   
    password: 

    yup
    .string()
    .matches(/\w*[a-z]\w*/,  "Password must have a small letter")
    .matches(/\w*[A-Z]\w*/,  "Password must have a capital letter")
    .matches(/\d/, "Password must have a number")
    // .matches(/[!@#$%^&*()\-_"=+{}; :,<.>]/, "Password must have a special character")
    .min(8, ({ min }) => `Password must be at least ${min} characters`)
    .required('Password is required'), 

    Confirmpassword: 
    yup
    .string()
    .oneOf([yup.ref('password')], 'Password do not match')
    .required("Confirmpassword is required"),
  })

export default function RegistrationScreen({navigation}){
    const [emailError, setEmailError] = useState(null)
    const [passwordError, setPasswordError] = useState(null)
    const [loading, setLoading] = useState()

    const createUser = async (values) => {
       setLoading(true)
        let ref1 = Firebase.database().ref().child('users').push()
        // let ref1 = Firebase.firestore().collection('users').add()
        ref1.set(values)
        Firebase.auth().createUserWithEmailAndPassword(values.email, values.password)
        .then((userCredential) => {
          var user = userCredential.user;
          var obj = {uid: user.uid, ...values}
          
          Alert.alert('Successful Registration!!!')
          navigation.navigate('LoginScreen',obj)      
        })
        .catch((error) => {
          var errorCode = error.code;
          var errorMessage = error.message;
          console.log(errorMessage)
          var error = errorMessage.includes("Password")
          if(error){
            setPasswordError(errorMessage)        
            setEmailError(null)
            console.log(error)
          } else{
            setPasswordError(null)        
            setEmailError(errorMessage)
            console.log(error)
          }
             setLoading(false)
        });
}  
return (
  <ScrollView>
    <View style={{width:'100%', marginTop: '20%'}}>
      <View style={{alignItems: 'center',justifyContent: 'center'}}>
        <Header>Sign Up</Header>
      </View>
      <View style={styles.container}>
        <Formik
          validationSchema = {reviewSchema}
          initialValues={{
            username: '',
            email: '',
            password: '',
            Confirmpassword: '',
          }}
          onSubmit = {(values,actions) => {
            actions.resetForm()
            createUser(values)
          }}
        >
        {(props)=>(
          <View>
          <Textinput
            label='Username'
            onChangeText = {props.handleChange('username')}
            value = {props.values.username}      
            autoCapitalize='none' 
          />
          <Text style={styles.errorText}>{props.touched.username && props.errors.username}</Text>

          <Textinput
            label='Email'
            onChangeText = {props.handleChange('email')}
            value = {props.values.email}
            onBlur = {props.handleBlur('email')}   
            autoCapitalize='none'         
          />
          {emailError ? 
            <Text style={styles.errorText}>{emailError}</Text>
              :null
          }
        <Text style={styles.errorText}>{props.touched.email && props.errors.email}</Text>

        <Textinput
          label='Password'
          onChangeText = {props.handleChange('password')}
          value = {props.values.password}
          onBlur = {props.handleBlur('password')}
          secureTextEntry
        />
        {passwordError ? 
          <Text style={styles.errorText}>{passwordError}</Text>
            :null
        }
        <Text style={styles.errorText}>{props.touched.password && props.errors.password}</Text>

        <Textinput
          label='Confirmpassword'
          onChangeText = {props.handleChange('Confirmpassword')}
          value = {props.values.Confirmpassword}
          onBlur = {props.handleBlur('Confirmpassword')}
          secureTextEntry
        />
        {passwordError ? 
          <Text style={styles.errorText}>{ConfirmpasswordError}</Text>
            :null
        }
        <Text style={styles.errorText}>{props.touched.Confirmpassword && props.errors.Confirmpassword}</Text>
        <View style={styles.row}>
          <Checkingbox />
          <TouchableOpacity onPress={()=>navigation.replace("TermsScreen")}>
            <Text style={styles.terms}>Terms and Ploicy</Text>
          </TouchableOpacity>
        </View>
        <Button 
          mode='contained'
          onPress={props.handleSubmit}
          icon='login'
          loading={loading}
        >Create Account</Button>
          <View style={styles.row}>
              <Text>Already have an account?</Text>
              <TouchableOpacity onPress={()=>navigation.replace("LoginScreen")}>
                  <Text style={styles.link}>  Login</Text>
              </TouchableOpacity>
          </View> 
        </View>
        )}    
        </Formik>
      </View>
    </View>
  </ScrollView>
);
}
const styles = StyleSheet.create({
    container: {
      alignSelf:'center',
      width:'90%',
    },
    row:{
        flexDirection:'row',
        marginTop:4,
        justifyContent:'center',
    },
    errorText: {
        color: 'red',
    },
    link:{
        fontWeight:'bold',
        color:theme.colors.primary,
    },
    terms: {
      color: '#F26349',
    },
    scrollView: {
      alignItems: 'center',
      justifyContent: 'center'
    }
  });