import {  Image  } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React, { useEffect, useState } from "react";
import RegistrationScreen from "./src/screens/RegistrationScreen/RegistrationScreen";
import LoginScreen from "./src/screens/LoginScreen/LoginScreen";
import ResetPasswordScreen from "./src/screens/ResetScreen/ResetPasswordScreen";
import { HomePage, TermsScreen, Library,MyPost, MenuScreen, NotificationScreen, ProfileScreen, Search,SearchScreen, WriteScreen } from "./src/screens";
import Add from './src/Components/add/Add'
import Save from './src/Components/add/Save'
import { LoadingScreen } from "./src/screens";
import CreateNewLibrary from './src/Components/CreateNewLibrary'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { LogBox } from 'react-native';
import { MyArticles } from "./src/screens";
import { FullImage } from "./src/screens";
import SearchBar from "./src/Components/SearchBar";
import UserDetailScreen from './src/Components/UserDetailScreen'
LogBox.ignoreLogs(['Setting a timer']);
LogBox.ignoreLogs(['Require cycle:']);
LogBox.ignoreLogs(['componentWillReceiveProps has been renamed']);
LogBox.ignoreLogs(['Animated:']);
LogBox.ignoreLogs(["Can't perform a React state update on an unmounted component:"]);

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName='LoadingScreen'
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="LoadingScreen"  component={LoadingScreen}/>
        <Stack.Screen name="LoginScreen" component={LoginScreen} />      
        <Stack.Screen name="RegistrationScreen" component={RegistrationScreen} />
        <Stack.Screen name="ResetPasswordScreen" component={ResetPasswordScreen} />
        <Stack.Screen name="TermsScreen" component={TermsScreen} />
        <Stack.Screen name="HomePage" component={BottomNavigation} />
        <Stack.Screen name="Library" component={BottomNavigation} />
        <Stack.Screen name="MenuScreen" component={MenuScreen}/>
        <Stack.Screen name="NotificationScreen" component={BottomNavigation} />
        <Stack.Screen name="ProfileScreen" component={ProfileScreen} />
        <Stack.Screen name="SearchScreen" component={BottomNavigation} />
        <Stack.Screen name="Search" component={Search} />
        <Stack.Screen name="SearchBar" component={SearchBar} />
        <Stack.Screen name="WriteScreen" component={BottomNavigation} />
        <Stack.Screen name="Add" component={Add} />
        <Stack.Screen name="Save" component={Save} />
        <Stack.Screen name="CreateNewLibrary" component={CreateNewLibrary} />
        <Stack.Screen name="MyPost" component={MyPost} />
        <Stack.Screen name="MyArticles" component={MyArticles} />
        <Stack.Screen name="FullImage" component={FullImage} />
        <Stack.Screen name="UserDetailScreen" component={UserDetailScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

function BottomNavigation() {
  return (
    <Tab.Navigator>
      <Tab.Screen 
      name='Home' 
      component={HomePage}
      options={{
        tabBarIcon: ({size}) => {
          return (
            <Image
            style={{ width: size, height: size }}
            source={require('./assets/Home-icon.png')} />
          );
        },
      }}
      />
      <Tab.Screen 
        name='Search' 
        component={SearchScreen} 
        screenOptions={{headerShown: false}}
        options={{
          tabBarIcon: ({size}) => {
            return (
              <Image
                style={{ width:size, height: size }}
                source={require('./assets/search-icon.png')} />
            )
          }
        }}/>
      <Tab.Screen 
        name='My Library' 
        component={Library} 
        options={{
          tabBarIcon: ({size}) => {
            return (
              <Image
                style={{ width:size, height: size }}
                source={require('./assets/library-icon.png')} />
            )
          }
        }}/>
      <Tab.Screen 
        name='Write' 
        component={WriteScreen} 
        options={{
          tabBarIcon: ({size}) => {
            return (
              <Image
                style={{ width:size, height: size }}
                source={require('./assets/write.png')} />
            )
          }
        }}/>
      <Tab.Screen 
        name='Notification' 
        component={NotificationScreen} 
        options={{
          tabBarIcon: ({size}) => {
            return (
              
              <Image
                style={{ width:size, height: size }}
                source={require('./assets/notification-icon.png')} />
            )
          }
        }}/>
    </Tab.Navigator>
  )
}