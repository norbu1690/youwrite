import React from 'react';
import {View, Text, StyleSheet, Image, SafeAreaView  } from 'react-native';
import TextAnimation from '../Components/TextAnimation';

const LoadingScreen = ({navigation}) => {
    setTimeout(()=>{
        navigation.navigate('LoginScreen')
    }, 3000)
  return ( 
      <SafeAreaView style={{flex:1, paddingTop: '-10%'}}>
        <View style={{flex:1, justifyContent: 'center', alignItems: 'center',}}>
            <Image size={60}  source ={require("../../assets/logo.png")} style={styles.image} resizeMode= "contain" />
            <View styly={styles.container}>
            <TextAnimation
            content = 'LET THE WRITE BEGIN'
            textStyle={styles.textStyle}
            styly={styles.containerStyle}
            duration= {2000}
            />
            </View>
        </View>
      </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  StartupContainer: {
    flex:1,
    height:'100%',
    width: '100%'
    
  },
  image: {
    width:'80%',
    height: '40%',
    bottom: '3%',
},
container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ECF0F1',
    padding: 8,
},
containerStyle: {
},
textStyle: {
    fontSize: 35,
    fontWeight: 'bold',
    margin: 4,
    color: '#F26349',
}
});

export default LoadingScreen;