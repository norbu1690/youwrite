import React, {useState}from 'react';
import {View, StyleSheet, Text, TouchableOpacity,ScrollView} from 'react-native';
import Header from '../../Components/Header';
import Textinput from '../../Components/Textinput'
import Button from '../../Components/Button';
import Background from '../../Components/Background';
import { theme } from '../../core/theme'; 
import { Formik } from 'formik';
import * as yup from 'yup';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Firebase from '../../Firebase/config';
import Logo from '../../Components/Logo';

const reviewSchema=yup.object({
    email:
    yup
    .string()
    .email('Please enter vaild email')
    .required('Email is required'),
    password:
    yup
    .string()
    .required('Password is required'),
})

export default function LoginScreen({navigation}){
    const [emailError, setEmailError] = useState(null)
    const [passwordError, setPasswordError] = useState(null)
    const [loading, setLoading] = useState();

    const storeData = async (value) => {
      setLoading(true)
        try {
          await AsyncStorage.setItem('user', value)
          navigation.navigate('HomePage')
          console.log('gs')
        } catch (e) {
        }
      }
    
      const LoginUser = async (values) => {
        Firebase.auth().signInWithEmailAndPassword(values.email, values.password)
        .then((userCredential) => {
          var user = userCredential.user;
          console.log(user.uid)
          storeData(user.uid)
        })
     
        .catch((error) => {
          var errorCode = error.code;
          var errorMessage = error.message;
          var error = errorMessage.includes("password")
            console.log(errorMessage)
          if(error){
            setPasswordError('The Password is Invalid')        
            setEmailError(null)
            console.log(error)
          } else{
            setEmailError(`${values.email} is not registered email`)        
            setPasswordError(null)        
            console.log(error)
          }
        setLoading(false)
        });
      }
    return (
          <ScrollView>
          <View style={styles.v1}>
            <View style={styles.v2}>
            <Logo />
            <Header>Sign In</Header>
            </View>
            <Formik
              validationSchema = {reviewSchema}
              initialValues={{
                email: '',
                password: '',
              }}
              onSubmit = {(values, actions) => {
              actions.resetForm()
              LoginUser(values)
              }}>
          {(props)=>(
        <View>
          <Textinput
            onChangeText = {props.handleChange('email')}
            value = {props.values.email}          
            label='Email'
            autoCapitalize='none'
          />
          {emailError ? 
            <Text style={styles.errorText}>{emailError}</Text>
            :
            null
          }
          <Text style={styles.errorText}>{props.touched.email && props.errors.email}</Text>
         
          <Textinput
            onChangeText = {props.handleChange('password')}
            value = {props.values.password}
            label='Password'
            secureTextEntry
            style={styles.input}
          />
          {passwordError ? 
            <Text style={styles.errorText}>{passwordError}</Text>
            :
            null
          }
          <Text style={styles.errorText}>{props.touched.password && props.errors.password}</Text>
          <View style={styles.forgotPassword}>
                <TouchableOpacity 
                    onPress={()=>navigation.navigate("ResetPasswordScreen")}>
                    <Text style={styles.forgot}> Forgot Your password? </Text>
                </TouchableOpacity>
            </View>

          <Button 
            loading = {loading}
            mode='contained'
            onPress={props.handleSubmit}
            icon='login'
          >Login</Button>
          <View
            style={{marginTop: 10}}>
              <View style={styles.row}>
                    <Text>Don't have an account? </Text>
                    <TouchableOpacity onPress={()=>navigation.replace("RegistrationScreen")}>
                        <Text style={styles.link}>  Sign up</Text>
                    </TouchableOpacity>
                </View>           
          </View>
        </View>
        )}
      </Formik>           
      </View>
      </ScrollView>  
    )
}
const styles = StyleSheet.create({
    row:{
        flexDirection:'row',
        marginTop:4,
        justifyContent:'center'
    },
    link:{
        fontWeight:'bold',
        color:theme.colors.primary
    },
    forgotPassword:{
        width:'100%',
        alignItems:'flex-end',
        marginBottom:24,
    },
    forgot:{
        fontSize:13,
        color:theme.colors.secondary,
    },
    errorText: {
        color: 'red',
      },
      input:{
        width:'100%'
      },
      v1: {
        width:'90%',
        margin:'5%',
        justifyContent: 'center',
      },
      v2: {
        marginTop:'15%',
        alignItems: 'center',
        justifyContent:'center'
      },
  });