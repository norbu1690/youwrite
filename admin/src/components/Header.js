import React,{useEffect, useState} from 'react'
import {Link, useLocation} from 'react-router-dom';
import './Header.css'

const Header = () => {
    const [activeTab, setActiveTab] = useState("Dashboard");
    const location = useLocation();

    useEffect(()=> {
        if(location.pathname==="/") {
            setActiveTab("Dashboard")
        }
        else if (location.pathname === "/add") {
            setActiveTab("Adduser")
        }
      

    },[location]);
  return (
    <div className='header'>
        <p className='logo'>Welcome to Youwrite Admin</p>
        <div className='header-right'>
         
        
           
        </div>
    </div>
  )
}

export default Header