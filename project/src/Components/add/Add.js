import { View, Text, TouchableOpacity, StyleSheet, Image, ScrollView } from 'react-native'
import React, { useState, useEffect } from 'react'
import { Camera } from 'expo-camera'
import * as ImagePicker from 'expo-image-picker'
import { SafeAreaView } from 'react-native-safe-area-context';
import Button from '../Button'

export default function Add({navigation}) {
  const [hasGalleryPermission, setHasGalleryPermission] = useState(null);
  const [hasCameraPermission, setHasCameraPermission] = useState(null);
  const [camera, setCamera] = useState(null);
  const [image, setImage] = useState(null);
  const [type, setType] = useState(Camera.Constants.Type.back)

  useEffect(() => {
    (async () => {
      const cameraStatus = await Camera.requestCameraPermissionsAsync();
      setHasCameraPermission(cameraStatus.status === 'granted');

      const galleryStatus = await ImagePicker.requestCameraPermissionsAsync();
      setHasGalleryPermission(galleryStatus.status === 'granted');

    })();
  }, []);

  const takePicture = async () => {
    if(camera) {
      const data = await camera.takePictureAsync(null);
      // console.log(data.uri)
      setImage(data.uri);
    }
  }

  const pickImage = async () => {
    let result  = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect : [1, 1],
      quality: 1,
    });
    console.log(result);
    
    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

  if (hasCameraPermission === false || hasGalleryPermission === false) {
    return <View />;
  }
  if (hasCameraPermission === false || hasGalleryPermission === false) {
    return <Text>No access to camera</Text>;
  }
  return (
      <SafeAreaView style={styles.safeArea}>
        <View style={{ flex: 1 }}>
          <View style={styles.cameraContainer}>
            <Camera 
              ref={ref => setCamera(ref)}
              style={styles.fixedRatio} 
              type={type}
              ratio={'1:1'} />
          </View>

          <Button
            icon='sync'
            onPress={() => {
              setType(
                type === Camera.Constants.Type.back
                  ? Camera.Constants.Type.front
                  : Camera.Constants.Type.back
              );
            }}>Flip Camera
          </Button>
          <TouchableOpacity onPress={() => takePicture()}>
           <Button icon='camera'>TAKE Picture</Button>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => pickImage()}>
            <Text style={{textDecorationLine:'underline', color: '#F26349', textAlign:'center', fontSize: 15, fontWeight:'bold'}}>PICK IMAGE FROM GALLERY{'\n'}</Text>
          </TouchableOpacity>
          {image && <Image source={{uri: image}} style={{ flex: 1 }} />}
        </View>
        <View style={{alignItems: 'center', marginTop:10}}>
          <Button onPress={()=> navigation.navigate("Save", {image})} icon ='safe'>Save</Button>
          <Button onPress={()=>navigation.replace("ProfileScreen")} icon='cancel'>Cancel</Button>
        </View>
      </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  cameraContainer: {
    flex: 1,
  },
  fixedRatio: {
    aspectRatio: 1,
    borderRadius: 30
  },
  safeArea: {
    flex: 1,
    padding: 10,
    maxHeight: '90%'
  }
})

