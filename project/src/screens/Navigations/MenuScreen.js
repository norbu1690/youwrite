import React from "react";
import Header from "../../Components/Header";
import Button from "../../Components/Button";
import { TouchableOpacity, StyleSheet, Image, View, Text, Alert } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";

export default function MenuScreen({navigation}) {
    const exit = () => {
        Alert.alert(
            'Log Out',
            'Are you sure?',
            [
              {text: 'Yes', onPress: () => navigation.navigate('LoginScreen')},
              {text: 'No', onPress: () => console.log('Canceled'), style: 'cancel'},
            ],
            { 
              cancelable: true 
            }
          );  
    }
    return (
        <SafeAreaView style={styles.container}>
        <View style={styles.container}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <TouchableOpacity onPress={()=>navigation.navigate("HomePage")}>
                    <Image
                        style={styles.imageBack}
                        source={require('../../../assets/arrow_back.png')} />
                </TouchableOpacity>
                <Header>Menu</Header>
            </View>
            <View style={styles.v1}>
            </View>
            <View style={styles.v3}>
                <Text style={styles.text}>Shortcut</Text>
            </View>
            <View style={styles.v4}>
                <TouchableOpacity onPress={()=>navigation.navigate("HomePage")}>
                    <View style={styles.item}>
                        <Image
                            style={styles.shortcutImg}
                            source={require('../../../assets/Home-icon.png')} />
                            <Text>HOME</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>navigation.replace("ProfileScreen")}>
                    <View style={styles.item}>
                        <Image
                                style={styles.shortcutImg}
                                source={require('../../../assets/profile-icon.png')} />
                                <Text>PROFILE</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={styles.v4}>
                <TouchableOpacity onPress={()=>navigation.replace("MyPost")}>
                    <View style={styles.item}>
                        <Image
                            style={styles.shortcutImg}
                            source={require('../../../assets/mypost.png')} />
                            <Text>MY POST</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>navigation.replace("CreateNewLibrary")}>
                    <View style={styles.item}>
                            <Image
                                style={styles.shortcutImg}
                                source={require('../../../assets/library-icon.png')} />
                                <Text>NEW LIBRARY</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={styles.btnView}>
                <Button 
                    mode='contained'
                    // onPress={() => {
                    //     logoutUser()}}
                    onPress={exit}
                    icon="logout"
                    >Log Out</Button>
            </View>
    
        </View>
        </SafeAreaView>
    )
}
const styles = StyleSheet.create ({
    container: {
        margin: 5,
    },
    btn: {
        width: '100%',
        marginVertical: 10,
        paddingVertical: 2,
        backgroundColor: '#F26349'
    },
    btnView: {
        flexDirection: 'row',
        marginTop: '15%',
    },
    text: {
        fontWeight: 'bold',
        fontSize: 18,
        lineHeight: 26,
        color: 'black'
    },
    image: {
        width: 20,
        height: 20,
    },
    imageBack: {
        width: 25,
        height: 25,
        marginRight: 10
    },
    v1: {
        flexDirection: 'row',
        alignItems: 'center',
 
    },
    v2: {
        flexDirection: 'row',
        alignItems: 'center',  
    },
    v3: {
        padding: 10
    },
    v4: {
        flexDirection: 'row',
    },
    item: {
        borderRadius: 15,
        height: 150,
        width: 154,
        margin: '3%',
        alignItems: 'center',
        backgroundColor: '#d2d4d6',
        shadowColor:"#000",
        shadowOffset: { width:0, height:2 }, 
        shadowOpacity:0.25, 
        shadowRadius:3.84, 
        elevation:10,
    },
    shortcutImg: {
        width: '50%',
        height: '50%',
        marginTop: 25
    }
})