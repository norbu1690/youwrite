import React, { Component } from 'react';
import { StyleSheet, ScrollView, ActivityIndicator, View, SafeAreaView, Image, Text, TouchableOpacity } from 'react-native';
import { ListItem } from 'react-native-elements'
import firebase from '../../Firebase/config';
import TouchableScale from 'react-native-touchable-scale'; 
import {LinearGradient} from 'expo-linear-gradient';

class MyPost extends Component {
  constructor() {
    super();
    this.firestoreRef = firebase.firestore().collection('Article');
    this.state = {
      isLoading: true,
      userArr: []
    };
  }
  componentDidMount() {
    this.unsubscribe = this.firestoreRef.onSnapshot(this.getCollection);
  }
  componentWillUnmount(){
    this.unsubscribe();
  }
  getCollection = (querySnapshot) => {
    const userArr = [];
    querySnapshot.forEach((res) => {
      const { title, content } = res.data();
      userArr.push({
        key: res.id,
        res,
        title,
        content,
      });
    });
    this.setState({
      userArr,
      isLoading: false,
   });
  }
  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    } 
    return (
      <SafeAreaView style={{flex:1, width: '90%', justifyContent: 'center', alignSelf: 'center', marginTop: 40}}>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity onPress={()=>this.props.navigation.replace("ProfileScreen")}>
            <Image
                style={styles.imageBack}
                source={require('../../../assets/arrow_back.png')} />
          </TouchableOpacity>
          <Text style={{fontSize: 20, marginLeft: 10, fontWeight: 'bold'}}>My Post</Text>
        </View>
        <ScrollView style={styles.container}>
            {
              this.state.userArr.map((item, i) => {
                return (
                  <ListItem
                    key={i}
                    chevron
                    bottomDivider
                    title={
                      <View><Text>Title: {item.title}</Text></View>
                    }
                    subtitle={
                      <View style={styles.subtitleView}>
                        <Image source={require('../../../assets/nature.jpeg')} style={{width: '100%',height: 100}}/>
                      </View>
                    }
                    leftAvatar={{
                      source: item.avatar_url && { uri: item.avatar_url },
                      title: item.title[0]
                    }}
                    onPress={() => {
                      this.props.navigation.navigate('MyArticles', {
                        userkey: item.key
                      });
                    }}
                    style={{margin: 5}}
                    Component={TouchableScale}
                    friction={90} //
                    tension={100} 
                    activeScale={0.95} //
                    linearGradientProps={{
                      colors: ['#FF9800', '#F44336'],
                      start: [1, 0],
                      end: [0.2, 0],
                    }}
                    ViewComponent={LinearGradient}
                    />
                );
              })
            }

        </ScrollView>
      </SafeAreaView>

    );
  }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingBottom: 22,
        width: '100%',
        alignSelf: 'center',
        margin: 20,
    },
    preloader: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center'
    },
    headcontainer: {
        justifyContent: 'flex-end',
        width: '100%',
        flexDirection: 'row',
        height: '7%'
    },
    image: {
        width: 30,
        height: 30,
        marginRight: 10,
        opacity: 1
    },
    imageBack: {
        width: 25,
        height: 25,
    },
    
})
export default MyPost;