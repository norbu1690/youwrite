import firebase from '../Firebase/config'
import 'firebase/auth'
import { getAuth, sendPasswordResetEmail } from "firebase/auth";
import { Alert } from 'react-native'

export function logoutUser(){
    Alert.alert(
        'Logging Out',
        'Are you sure?',
        [
          {text: 'Yes', onPress: () => firebase.auth().signOut()},
          {text: 'No', onPress: () => console.log('Canceled'), style: 'cancel'},
        ],
        { 
          cancelable: true 
        }
      );   
    // firebase.auth().signOut();
}

export async function resetPassword(email){
    try{
        await firebase
            .auth()
            .sendPasswordResetEmail(email);
        return {};
    }
    catch  (error){
        return{
            error: error.message,
        }
    }
}