import React,{useState,useEffect} from 'react'
import { StyleSheet, Image,Text, TouchableOpacity, View, SafeAreaView, Alert, FlatList, Keyboard, ScrollView } from "react-native";
import { TextInput } from "react-native-paper";
import Button from "../../Components/Button";
import { MaterialIcons } from '@expo/vector-icons';
import firebase from '../../Firebase/config';
import { FontAwesome } from "@expo/vector-icons";
import styles from '../../Components/styles';
import { getStorage, ref, getDownloadURL } from 'firebase/storage'
import Header from '../../Components/Header';
import FetchUser from '../../Components/FetchUser';

export default function ProfileScreen({navigation}) {
    const [info, setInfo] = useState(''); 
    const [information, setInformation] = useState([]);
    const infoRef = firebase.firestore().collection('information')
    const [url, setUrl] = useState();

    useEffect(() => {
        infoRef
            .orderBy('createdAt', 'desc')
            .onSnapshot(
                querySnapshot => {
                    const newInformation = []
                    querySnapshot.forEach(doc => {
                        const info = doc.data()
                        info.id = doc.id
                        newInformation.push(info)
                    });
                    setInformation(newInformation)
                },
                error => {
                    console.error(error);
                }
            )
    }, []);

    const addInfo = () => {
        if (info && info.length > 0) {
            const timestamp = firebase.firestore.FieldValue.serverTimestamp();
            const data = {
                text: info,
                createdAt: timestamp
            };
            infoRef
                .add(data)
                .then(() => {
                    setInfo('');
                    Keyboard.dismiss();
                })
                .catch((error) => {
                    alert(error);
                })
        }
    }

    const deleteInfo = (info) => {
       
        infoRef
            .doc(info.id)
            .delete()
            .then(() => {
                alert("Deleted successfully");
            })
            .catch(error => {
                alert(error);
            })
    }

    const renderInfo = ({ item }) => {
        return (
            <View>
                <Text style={{color:'black', textAlign: 'center'}}>
                    {item.text[0].toUpperCase() + item.text.slice(1)}
                </Text>
            </View>
        )
    }
    const renderDel = ({ item }) => {
        return (
            <View style={{flexDirection: 'row'}} >
                <View style={{}}>
                    <FontAwesome name="trash-o" color="red" size={22} onPress={() => deleteInfo(item)} style={{marginLeft: 3, marginBottom: 15}} />
                </View>
                <View><Text style={{ fontSize:16, color:'#F26349' }} onPress={() => deleteInfo(item)}>     Delete Bio</Text></View>
            </View>
        )
    }

    useEffect(() => {
        const func = async () => {
        const storage = getStorage();
        const reference = ref(storage, 'https://firebasestorage.googleapis.com/v0/b/youwrite-9af11.appspot.com/o/post%2F%24%7Bfirebase.auth().currentUser.uid%7D%2F%24%7BMath.random().toString(36)%7D?alt=media&token=8ae8c95e-e0ca-4dd6-82d1-a7a0be43e04f');
        await getDownloadURL(reference).then((x) => {
            setUrl(x);
        })
        }

        if (url == undefined) {func()};
        }, []);

        const exit = () => {
        Alert.alert(
            'Log Out',
            'Are you sure?',
            [
            {text: 'Yes', onPress: () => navigation.navigate('LoginScreen')},
            {text: 'No', onPress: () => console.log('Canceled'), style: 'cancel'},
            ],
            { 
            cancelable: true 
            }
        );  
        }
    return (
        <View style={{height: '100%', backgroundColor: '#edafa4', marginBottom: 0}}>
        <SafeAreaView style={style.container}>
            <View style={style.container}>
                <View style={{flexDirection: 'row', alignItems: 'center', margin: 20, marginTop: 30}}>
                    <TouchableOpacity onPress={()=>navigation.navigate("HomePage")}>
                        <Image
                            style={style.imageBack}
                            source={require('../../../assets/arrow_back.png')} />
                    </TouchableOpacity>
                    <Header>Profile</Header>
                </View>
                <View style={style.v1}>
                    <TouchableOpacity onPress={() => navigation.navigate('FullImage')} >
                    <Image
                        style={style.profilePic}
                        source={{ uri: url }}
                        alt='No Profile'
                    />
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={.7}
                        onPress={()=>navigation.replace("Add")}
                        style={{position: 'absolute', bottom: '25%', right:'30%'}}
                    >
                        <MaterialIcons name="camera-enhance" size={50} color= 'black' />
                    </TouchableOpacity>   
                    <FetchUser />
                </View>
                <View style={{minHeight: '10%', width: '100%', paddingRight: 35, paddingLeft: 30, alignContent: 'center'}}>
                    {information.length > 0 && (
                        <View style={styles.listContainer}>
                            <FlatList
                                data={information}
                                renderItem={renderInfo}
                                keyExtractor={(info) => info.id}
                                removeClippedSubviews={true}
                            />
                        </View>
                    )}
                </View>
                <View style={style.mainView}>
                    <View style={style.v2}>
                        <View style={{width: '20%'}}>
                            <Text style={{color:'#F26349', textDecorationLine:'underline', fontWeight: 'bold', fontSize: 17}}>ABOUT</Text>
                        </View>
                        <View style={style.item}>
                            <TouchableOpacity onPress={()=>navigation.replace("Add")}>
                                <Button>Update Profile Picture</Button>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row', marginBottom: 5 }}>
                        <TextInput style={style.input}
                            underlineColor="transparent"
                            placeholder="Add a description about yourself..."
                            onChangeText={(text) => setInfo(text)}
                            value={info}
                        />
                            <TouchableOpacity style={styles.button} onPress={addInfo}>
                                <Text style={styles.buttonText}>ADD</Text>
                            </TouchableOpacity>
                    </View>
                 
                    <View style={{marginTop: 15}}>
                        <View style={{flexDirection: 'row', height: 30, }}>
                            {information.length > 0 && (
                                <View style={styles.listContainer}>
                                    <FlatList
                                        data={information}
                                        renderItem={renderDel}
                                        keyExtractor={(info) => info.id}
                                        removeClippedSubviews={true}
                                    />
                                </View>
                             )} 
                        </View>
                        <View style={{flexDirection: 'row', marginBottom:15}}>
                            <TouchableOpacity onPress={()=>navigation.replace("MyPost")}>
                                <MaterialIcons name="post-add" size={25} color= '#F26349' /> 
                            </TouchableOpacity>
                            <TouchableOpacity onPress={()=>navigation.replace("MyPost")}>
                                <Text style={{fontSize:16, color: '#F26349'}} >    My Post</Text> 
                            </TouchableOpacity>
                        </View>

                        <View style={{flexDirection: 'row', marginBottom:15}}>
                            <TouchableOpacity onPress={()=>navigation.replace("MyPost")}>
                                <MaterialIcons name="account-circle" size={25} color= '#F26349' /> 
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => navigation.navigate('FullImage')}>
                                <Text style={{fontSize:16, color: '#F26349'}} >    View Profile Picture</Text> 
                            </TouchableOpacity>
                        </View>

                        <View style={{flexDirection: 'row'}}>
                            <TouchableOpacity onPress={()=>navigation.replace("MyPost")}>
                                <MaterialIcons name="logout" size={25} color= '#F26349' />
                            </TouchableOpacity>
                            <TouchableOpacity  onPress={exit}>
                                <Text style={{fontSize: 16, color: '#F26349'}}>    Log Out</Text> 
                            </TouchableOpacity>
                        </View>
                    </View>  
                </View>
            </View>
        </SafeAreaView>
        </View>
    )
}

const style = StyleSheet.create ({
    container: {
        flex: 1,
        height: '100%'
    },
    imageBack: {
        width: 25,
        height: 25,
        marginRight: 10
    },
    mainView: {
        flex: 1,
        height: '100%',
        backgroundColor: '#e8e4e3',
        width:"100%",
        padding: 10,
        borderTopEndRadius: 50,
        borderTopLeftRadius: 50,    
    },
    v1: {
        height: '25%',
        alignItems: 'center',
        paddingBottom: 10
    },
    v2: {
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#e8e4e3',
        borderTopLeftRadius: 50,
        padding: 10,
        borderTopEndRadius: 50
    },
    profilePic: {
        width: 150,
        height: 150,
        borderRadius: 70,
    },
    item: {
        height: 85,
        width: 250,
        margin: 10,
        marginTop: 0,
        marginBottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    abt: {
        textDecorationLine: 'underline',
        color: '#F26349',
        fontWeight: 'bold',
        fontSize: 20 
    },
    input: {
        fontSize: 12,
        height: 45,
        width: 230,
        marginRight: 10
    },
})
