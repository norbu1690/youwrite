import { StyleSheet, Text, View, Image, TouchableOpacity,SafeAreaView, TextInput } from 'react-native'
import React, { useState } from 'react'
import Header from '../Header'
import firebase from '../../Firebase/config'
import Button from '../Button'

export default function Save(props, {navigation}) {
  // console.log(props.route.params.image)
  const [caption, setCaption] = useState("") 
  const [loading, setLoading] = useState();

  const uploadImage = async() => {
    setLoading(true)
    const uri = props.route.params.image;   
    const childPath = 'post/${firebase.auth().currentUser.uid}/${Math.random().toString(36)}'
    console.log(childPath)
    const response = await fetch(uri);
    const blob = await response.blob();

    const task = firebase
      .storage()
      .ref()
      .child(childPath)
      .put(blob);
    
    const taskProgress = snapshot => {
      console.log('transferred: ${snapshot.bytesTransferred}')
    }

    const taskCompleted = ()=> {
      task.snapshot.ref.getDownloadURL().then((snapshot) => {
        savePostData(snapshot);
        console.log(snapshot)
      })
    }

    const taskError = snapshot => {
      console.log(snapshot)
    }

    task.on("state_changed", taskProgress, taskError, taskCompleted);
   
  }

  const savePostData =  (downloadURL) => {
    
    firebase.firestore()
      .collection('posts')
      .doc(firebase.auth().currentUser.uid)
      .collection("userPosts")
      .add({
        downloadURL,
        caption,
        creation: firebase.firestore.FieldValue.serverTimestamp()
      })
      .then((function () {
        // props.navigation.popToTop()
        alert('Successfully uploaded')
        props.navigation.navigate('ProfileScreen')
      }))
      setLoading(false)
  }

  const cancel =  () => {
    
  
        props.navigation.navigate('ProfileScreen')
     
  }

  return (
    <View style={{ flex: 1, margin: 15, marginTop:45 }}>
      <Header>SaveScreen</Header>
      <Image source={{uri: props.route.params.image}}/>
        <Button
         icon='safe'
          onPress={() => uploadImage()}
          loading = {loading}
        >Save</Button>
         <Button
         icon='cancel'
          onPress={() => cancel()}
        >cancel</Button>
    </View>
  )
}

const styles = StyleSheet.create({
  imageBack: {
    width: 25,
    height: 25,
},
})
