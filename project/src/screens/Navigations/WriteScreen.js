import React, { Component } from 'react';
import { StyleSheet, ScrollView,TextInput, ActivityIndicator, View } from 'react-native';
import firebase from '../../Firebase/config';
import Button from '../../Components/InnerButton';

class WriteScreen extends Component {
  constructor() {
    super();
    this.dbRef = firebase.firestore().collection('Article');
    this.state = {
      title: '',
      content: '',
      isLoading: false
    };
  }

  handleReset() {
    this.setState({ title: '', content: '' })
  }

  inputValueUpdate = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }
  storeArticle() {
    if((this.state.title === '') && (this.state.content === '')){
      alert('You cannot have null post.')
    } 
    else if((this.state.title === '')) {
      alert('Title is must.')
    }
    else if((this.state.content === '')) {
      alert('You should have some content.')
    }
    else {
      this.setState({
        isLoading: true,
      });      
      this.dbRef.add({
        title: this.state.title,
        content: this.state.content,
      }).then((res) => {
        this.setState({
          title: '',
          content: '',
          isLoading: false,
        });
        alert('Upload successful')
        this.props.navigation.navigate('HomePage')
      })
      .catch((err) => {
        console.error("Error found: ", err);
        this.setState({
          isLoading: false,
        });
      });
    }
  }
  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }
    return (
        <View style={{height: '100%', padding:5}}>
          <View style={styles.inputGroup}>
            <TextInput
                placeholder={'Title'}
                value={this.state.title}
                style={{height: 20}}
                onChangeText={(val) => this.inputValueUpdate(val, 'title')}
            />
          </View>
          <View style={styles.contentView}>
            <TextInput
                multiline={true}
                numberOfLines={30}
                placeholder={'You can write here...'}
                value={this.state.emcontentail}
                onChangeText={(val) => this.inputValueUpdate(val, 'content')}
                style={{textAlignVertical: 'top', maxHeight: '100%'}}
            />
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <Button onPress={() => this.handleReset()} >Clear</Button>
            <Button onPress={() => this.storeArticle()}>post</Button>
          </View>
        </View>
     
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  inputGroup: {
    padding:10,
    marginBottom: 15,
    borderWidth: 1,
    borderColor: '#cccccc',
    borderRadius: 10,
    height: 40
  },
  contentView: {
    padding:5,
    marginBottom: 15,
    borderWidth: 1,
    borderColor: '#cccccc',
    borderRadius: 10,
    height: '80%',
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  }
})
export default WriteScreen;
