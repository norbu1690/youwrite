import React, { useState } from "react";
import { StyleSheet, TextInput, View, TouchableOpacity } from "react-native";
import { FontAwesome5 } from "@expo/vector-icons";

const SearchScreen = ({navigation}) => {
  const [s, setS] = useState('')
  const Sen = () => {
    if (s=='') {
      alert('search cannot be empty!')
      return false;
    }
    else{
      navigation.replace('Search',s)
    }
  }
  return (
    <View style={{marginTop:30, flexDirection: 'row',backgroundColor:'#ebe6e6',width:'95%',marginLeft:10,borderRadius:10}}>
      <View style={styles.searchContainer}>
        <TextInput
        autoCapitalize='none'
        Style={{color:'black'}}
        onChangeText={(text)=> setS(text)}
        placeholder='search' />
      </View>
      <TouchableOpacity style={{marginRight:20,marginTop:10}} onPress={Sen}>
        <FontAwesome5 name="search" size={24} color="#2F3D4F" />
      </TouchableOpacity>
    </View>
  );
};

export default SearchScreen;

const styles = StyleSheet.create({
  root: {
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    width: "100%",
    marginTop: 20,
    fontSize: 25,
    fontWeight: "bold",
    marginLeft: "10%",
  },
  searchContainer: {
    height: 50,
    borderRadius: 10,
    flex: 1,
    backgroundColor:'#ebe6e6',
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 20,
  },
});


