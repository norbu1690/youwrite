import React, { useState } from "react";
import { View, StyleSheet, Text, TouchableOpacity, ScrollView } from "react-native";
import Button from "../../Components/Button";
import Header from "../../Components/Header";
import Background from "../../Components/Background";
import TextInput from "../../Components/Textinput";
import { emailValidator } from '../../core/helpers/emailValidator';
import BackButton from "../../Components/BackButton";
import { resetPassword } from "../../api/auth-api"

export default function ResetPasswordScreen({navigation}) {
    const [email, setEmail] = useState({ value: "", error: ""})
    const [loading, setLoading] = useState();

    const onSubmitPressed = async() => {
        const emailError = emailValidator(email.value);
        if (emailError) {
            setEmail({ ...email, error: emailError });
        }
        setLoading(true)

        const response = await resetPassword(email.value);

        if (response.error) {
            alert(response.error);
        }
        else {
            alert('Check your email for a reset link')
            navigation.navigate('LoginScreen') 
        }
        setLoading(false)
    }

    const onCancelPressed = () => {
        navigation.navigate('LoginScreen')
    }
    return (
        <View style={{flex:1, width:'100%', padding: 10, paddingTop: '30%'}}>
            <BackButton goBack={navigation.goBack} />
            <ScrollView style={{flex:1}}>
                
                <Header>Reset Password</Header>
                <TextInput 
                    label="Email" 
                    value={email.value}
                    placeholder='Enter email'
                    autoCapitalize='none'
                    error={email.error}
                    errorText={email.error}
                    onChangeText={(text) => setEmail({ value: text, error: "" })}
                    description="Password reset email sent successfully"
                />
                <Button loading = {loading} mode="contained" onPress = {onSubmitPressed}>Submit</Button> 
                <Button mode="contained" onPress = {onCancelPressed}>Cancel</Button>
            </ScrollView>
        </View>
    )
}
