
import React from "react";
import ReactDOM from "react-dom/client";
import { Routes, BrowserRouter as Router, Route} from "react-router-dom";
import './App.css';

import Dashboard from "./pages/Dashboard";
import View from "./pages/View";
import Layout from "./pages/layout";

import Header from "./components/Header";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";




export default function App() {
  // const root = ReactDOM.createRoot(document.getElementById('root'));
  return (
  
      <Router>
      <div>
      <Header position="top-center" />
      <ToastContainer position="top-center"/>
      <Routes > 
       {/* <Route path="/" element={<Layout />}> */}
          <Route path= "/" element={<Dashboard />} />
          
       
          <Route path="/view" element={<View />} />
      </Routes>
      </div>
    </Router>
  

    
  );
}


 
