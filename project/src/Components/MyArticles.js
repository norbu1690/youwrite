import React, { Component } from 'react';
import { Alert, StyleSheet, TextInput, ScrollView,SafeAreaView, ActivityIndicator, View } from 'react-native';
import firebase from '../Firebase/config';
import Button from './InnerButton'

class MyPost extends Component {
  constructor() {
    super();
    this.state = {
      title: '',
      content: '',
      isLoading: true
    };
  }
  componentDidMount() {
    const dbRef = firebase.firestore().collection('Article').doc(this.props.route.params.userkey)
    dbRef.get().then((res) => {
      if (res.exists) {
        const article = res.data();
        this.setState({
          key: res.id,
          title: article.title,
          content: article.content,
          isLoading: false
        });
      } else {
        console.log("Document does not exist!");
      }
    });
  }
  inputValueUpdate = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }
  updateUser() {
    this.setState({
      isLoading: true,
    });
    const updateDBRef = firebase.firestore().collection('Article').doc(this.state.key);
    updateDBRef.set({
      title: this.state.title,
      content: this.state.content,
    }).then((docRef) => {
      this.setState({
        key: '',
        title: '',
        content: '',
        isLoading: false,
      });
      Alert.alert('Success message!', 'Update successful')
      this.props.navigation.navigate('MyPost');
    })
    .catch((error) => {
      console.error("Error: ", error);
      this.setState({
        isLoading: false,
      });
    });
  }
  deleteUser() {
    const dbRef = firebase.firestore().collection('Article').doc(this.props.route.params.userkey)
      dbRef.delete().then((res) => {
          console.log('Item removed from database')
          this.props.navigation.navigate('MyPost');
      })
  }
  openTwoButtonAlert=()=>{
    Alert.alert(
      'Deleteing  an article',
      'Are you sure?',
      [
        {text: 'Yes', onPress: () => this.deleteUser()},
        {text: 'No', onPress: () => console.log('No item was removed'), style: 'cancel'},
      ],
      { 
        cancelable: true 
      }
    );
  }
  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }
    return (
        <View style={styles.container}>
          <View style={styles.inputGroup}>
            <TextInput
              style={{fontWeight: 'bold', fontSize: 18}}
              placeholder={'Title'}
              value={this.state.title}
              onChangeText={(val) => this.inputValueUpdate(val, 'title')}
            />
          </View>
          <View style={styles.contentView}>
            <TextInput
              multiline={true}
              numberOfLines={30}
              placeholder={'Content'}
              value={this.state.content}
              onChangeText={(val) => this.inputValueUpdate(val, 'content')}
              style={{textAlignVertical: 'top', maxHeight: '100%'}}
            />
          </View>
          <View style={styles.button}>
            <Button onPress={()=> this.props.navigation.navigate("MyPost")}>Back</Button>
            <Button onPress={() => this.updateUser()}>update</Button>
            <Button onPress={this.openTwoButtonAlert}>delete</Button> 
          </View>
        </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    marginTop: 30,
    height: '100%'
  },
  inputGroup: {
    padding:10,
    marginBottom: 15,
    borderWidth: 1,
    borderColor: '#cccccc',
    borderRadius: 10,
    height: '7%',
    flexDirection: 'row',
  },
  contentView: {
    padding:5,
    marginBottom: 15,
    borderWidth: 1,
    borderColor: '#cccccc',
    borderRadius: 10,
    height: '80%',  
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    marginBottom: 7, 
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    height: '9%',
  },
})
export default MyPost;