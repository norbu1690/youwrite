import React, { Component } from 'react';
import { StyleSheet, View, Text} from 'react-native';
import firebase from '../Firebase/config';
import { auth } from '../Firebase/config';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

class FetchUser extends Component {
  state ={
      text:'',
      userList: []
  }

  componentDidMount() {
    const user = firebase.database().ref('users');
    user.on("value", datasnap => {
        this.setState({userList:Object.values(datasnap.val()) })
    })
  }
  render() {

    const userList = this.state.userList.map(item => {
        return(
            <Text>{item.username}</Text>
        )
    })
    return ( 
        <View style={styles.container}>
                <Text> <Icon name="email" color='black' size={20} />  {auth.currentUser?.email}</Text>
        </View>  
    )
    }     
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    height: '100%',
    color: 'black',

  },
  
})
export default FetchUser;
